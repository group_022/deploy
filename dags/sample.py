from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.email_operator import EmailOperator 
from datetime import datetime, timedelta


default_args={
    'owner': 'akshay',
    'start_date': datetime(2024, 5, 10),
    'email': 'akshaydm555@gmail.com',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    dag_id = 'Sample_dag-01',
    start_date = datetime(2024, 5, 10),
    end_date = datetime(2024, 5, 15),
    default_args = default_args,
    catchup = False,
    schedule_interval = "@daily",
    description = "first try"
)

def hello():
    return f"Hello world!"

task1 = PythonOperator(
    task_id = "task1",
    python_callable = hello,
    dag = dag
)

task2 = BashOperator(
    task_id = 'task2',
    bash_command = 'echo "I am bash"',
    dag = dag
)

task3 = EmailOperator(
    task_id = "task3",
    to = 'akshaydm555@gmail.com',
    subject = 'subject',
    html_content = ' content of the mail',
    dag = dag
)

task2 >> task1 >> task3
